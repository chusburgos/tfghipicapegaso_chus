package main;

import gui.*;

public class Main {

    /**
     * Metodo principal que ejecutara toda la aplicacion
     * @author Maria Jesus Burgos Sanz on 12/04/2021
     *
     */
    public static void main(String[] args) {

        VistaLoginAdmin vistaLoginAdmin = new VistaLoginAdmin();
        VistaAdministrador vistaAdministrador = new VistaAdministrador();

        VistaLoginComercial vistaLoginComercial = new VistaLoginComercial();
        VistaRegistroComercial vistaRegistroComercial = new VistaRegistroComercial();
        VistaComercial vistaComercial = new VistaComercial();

        VistaLoginCliente vistaLoginCliente = new VistaLoginCliente();
        VistaCliente vistaCliente = new VistaCliente();

        Modelo modelo = new Modelo();
        VistaPrincipal vistaPrincipal = new VistaPrincipal();
        Controlador controlador = new Controlador(modelo, vistaPrincipal, vistaLoginAdmin, vistaAdministrador, vistaLoginComercial, vistaRegistroComercial, vistaComercial,
                vistaLoginCliente, vistaCliente);
    }
}
