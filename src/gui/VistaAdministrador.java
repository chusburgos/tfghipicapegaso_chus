package gui;

import enums.Disciplina;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class VistaAdministrador extends JFrame {

    private final static String TITULOFRAME = "ADMINISTRADOR: HIPICA PEGASO SL";
    public JPanel panelAdmin;
    public JTabbedPane tabbedPane1;


    /* SOCIO */
    public JTextField nombreSocioTxt;
    public JTextField apellidoSocioTxt;
    public JTextField dniSocioTxt;
    public JTextField direccionSocioTxt;
    public JTextField ciudadSocioTxt;
    public DatePicker fechaNacimientoSocioDate;
    public JTextField telefonoSocioTxt;
    public DatePicker fechaInscripcionSocioDate;
    public JTextField buscarTelefonoSocio;
     // botones
     public JButton anadirSocioBtn;
    public JButton borrarSocioBtn;
    public JButton modificarSocioBtn;
    public JButton buscarSocioBtn;
    public JButton volverDesdeSocioBtn;
    // tabla y lista de socios
    public JTable tablaSocio;
    public JTable tablaBusquedaDniSocio;

    /* PROFESOR */
    public JTextField nombreProfesorTxt;
    public JTextField apellidoProfesorTxt;
    public JTextField dniProfesorTxt;
    public JTextField direccionProfesorTxt;
    public JTextField ciudadProfesorTxt;
    public DatePicker fechaNacimientoProfesorDate;
    public JTextField telefonoProfesorTxt;
    public JComboBox<String> comboBoxDisciplinaProfesor;
    public JComboBox<String> comboBoxHorasProfesor;
    public JTextField buscarDniProfesor;
    // botones
    public JButton anadirProfesorBtn;
    public JButton borrarProfesorBtn;
    public JButton modificarProfesorBtn;
    public JButton buscarProfesorBtn;
    public JButton volverDesdeProfesorBtn;
    // tabla y lista de profesores
    public JTable tablaProfesor;
    public JTable tablaBusquedaDniProfesor;

    /* CLASES */
    public JComboBox<String> comboBoxNombreSocioClases;
    public DatePicker fechaReservaClaseDate;
    public JComboBox<String> comboBoxTipoClases;
    public JTextField cantClasesTxt;
    public JTextField metodoPagoTxt;
    public JComboBox<String> comboBoxDisciplinaClase;
    public JTextField precioClaseTxt;
    // botones
    public JButton anadirClaseBtn;
    public JButton borrarClaseBtn;
    public JButton modificarClaseBtn;
    public JButton generarFacturaBtn;
    public JButton volverDesdeClaseBtn;
     // tabla y lista de clases
     public JTable tablaClase;

    /* PROVEEDOR */
    public JTextField nombreProveedorTxt;
    public JTextField direccionProveedorTxt;
    public JTextField ciudadProveedorTxt;
    public JTextField emailProveedorTxt;
    public JTextField buscarCiudadProveedor;
    public DatePicker fechaContratoProveedorDate;
     // botones
     public JButton anadirProveedorBtn;
    public JButton borrarProveedorBtn;
    public JButton modificarProveedorBtn;
    public JButton buscarProveedorBtn;
    public JButton volverDesdeProveedorBtn;
    // tabla y lista de proveedores
    public  JTable tablaProveedor;
    public  JTable tablaBusquedaNombreProveedor;

    /* Default table models */
    public DefaultTableModel dtmSocios;
    public DefaultTableModel dtmProfesores;
    public DefaultTableModel dtmClases;
    public DefaultTableModel dtmProveedores;

    /* Default table models busqueda */
    public DefaultTableModel dtmSociosBusqueda;
    public DefaultTableModel dtmProfesoresBusqueda;
    public DefaultTableModel dtmProveedoresBusqueda;



    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaAdministrador() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hara visible cuando la llame el controlador
     */
    public void initFrame() {
        this.setContentPane(panelAdmin);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
        setEnumComboBox();
        anadirTipoClasesComboBox();
        anadirHorasProfesorComboBox();
        setTableModels();
        ocultarBordeBotones();
    }
    /**
     *  Metodo para ocultar los bordes de los botones -> pura estetica
     */
    private void ocultarBordeBotones() {
        buscarSocioBtn.setBorder(null);
        buscarProfesorBtn.setBorder(null);
        buscarProveedorBtn.setBorder(null);

        volverDesdeSocioBtn.setBorder(null);
        volverDesdeProfesorBtn.setBorder(null);
        volverDesdeClaseBtn.setBorder(null);
        volverDesdeProveedorBtn.setBorder(null);
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    public void setTableModels() {
        // SOCIOS
        this.dtmSocios = new DefaultTableModel();
        this.tablaSocio.setModel(dtmSocios);

        this.dtmSociosBusqueda = new DefaultTableModel();
        this.tablaBusquedaDniSocio.setModel(dtmSociosBusqueda);


        // PROFESORES
        this.dtmProfesores = new DefaultTableModel();
        this.tablaProfesor.setModel(dtmProfesores);

        this.dtmProfesoresBusqueda = new DefaultTableModel();
        this.tablaBusquedaDniProfesor.setModel(dtmProfesoresBusqueda);


        // CLASES
        this.dtmClases = new DefaultTableModel();
        this.tablaClase.setModel(dtmClases);


        // PROVEEDORES
        this.dtmProveedores = new DefaultTableModel();
        this.tablaProveedor.setModel(dtmProveedores);

        this.dtmProveedoresBusqueda = new DefaultTableModel();
        this.tablaBusquedaNombreProveedor.setModel(dtmProveedoresBusqueda);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        // Disciplina que tiene de especialidad el profesor
        for(Disciplina constant : Disciplina.values()) { comboBoxDisciplinaProfesor.addItem(constant.getValor()); }
        comboBoxDisciplinaProfesor.setSelectedIndex(-1);

        // Disciplina que se va impartir en la clase
        for(Disciplina constant : Disciplina.values()) { comboBoxDisciplinaClase.addItem(constant.getValor()); }
        comboBoxDisciplinaClase.setSelectedIndex(-1);

    }
    /**
     * Metodo que añade todos los datos en el comboBoxTipoClases
     * Los DATOS, se mostrara cuando se pulse mostrar lista de tipo de clases que ofrece la hipica.
     * El comboBoxTipoClases son las dos opciones que ofrece la hipica, puede ser individual o grupal.
     */
    private void anadirTipoClasesComboBox() {
        comboBoxTipoClases.addItem("Individual");
        comboBoxTipoClases.addItem("Grupal");
    }
    /**
     * Metodo que añade todos los datos en el comboBoxHorasProfesor
     * Los DATOS, se mostrara cuando se pulse mostrar lista de numero de horas que este contratado el profesor.
     * El comboBoxHorasProfesor son las horas de contrato que puede tener el profesor semanalmente
     */
    private void anadirHorasProfesorComboBox() {
        comboBoxHorasProfesor.addItem("12");
        comboBoxHorasProfesor.addItem("20");
        comboBoxHorasProfesor.addItem("40");
    }



}
