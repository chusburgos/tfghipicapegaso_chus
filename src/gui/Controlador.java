package gui;

import enums.Disciplina;
import hash.Hash;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.io.File;
import java.sql.*;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;

    private VistaPrincipal vistaPrincipal;

        /* VISTAS DEL CONTROL DE USUARIO */
    private VistaLoginAdmin vistaLoginAdmin;
    private VistaLoginComercial vistaLoginComercial;
    private VistaRegistroComercial vistaRegistroComercial;
    private VistaLoginCliente vistaLoginCliente;

        /* VISTAS DE CADA TIPO DE USUARIO*/
    private VistaAdministrador vistaAdministrador;
    private VistaComercial vistaComercial;
    private VistaCliente vistaCliente;



    boolean refrescar, iniciar, tieneClase, tienePedido, claseSeleccionada;

    /**
     * Constructor de la clase controlador
     * @param modelo en la clase modelos estan todos los metodos de consulta con la base de datos
     * @param vistaPrincipal accion de los 3 botones
     * @param vistaLoginAdmin inicio sesion del administrador unico
     * @param vistaAdministrador vista registro, filtrado y edicion
     * @param vistaLoginComercial inicio sesion del comercial
     * @param vistaRegistroComercial registro de un nuevo comercial
     * @param vistaComercial vista de los pedidos que se haran a los proveedores siendo un comercial
     * @param vistaLoginCliente inicio sesion por filtrado de la disciplina del cliente
     * @param vistaCliente vista de la disciplina seleccionada del cliente
     */
    public Controlador(Modelo modelo, VistaPrincipal vistaPrincipal, VistaLoginAdmin vistaLoginAdmin, VistaAdministrador vistaAdministrador,
                       VistaLoginComercial vistaLoginComercial, VistaRegistroComercial vistaRegistroComercial, VistaComercial vistaComercial,
                       VistaLoginCliente vistaLoginCliente, VistaCliente vistaCliente) {
        this.modelo = modelo;
        this.vistaPrincipal = vistaPrincipal;
        this.vistaLoginAdmin = vistaLoginAdmin;
        this.vistaAdministrador = vistaAdministrador;

        this.vistaLoginComercial = vistaLoginComercial;
        this.vistaRegistroComercial = vistaRegistroComercial;
        this.vistaComercial = vistaComercial;

        this.vistaLoginCliente = vistaLoginCliente;
        this.vistaCliente = vistaCliente;

        modelo.conectarHipica();
        addActionListeners(this);
        addWindowListeners(this);
        mouseTableListener();
        refrescarTodo();
        inicarTablas();
    }

    /**
     * Actualiza los datos de cada tabla
     */
    private void refrescarTodo() {
        refrescarSocios();
        refrescarProfesores();
        refrescarClases();
        refrescarProveedores();
        refrescarPedidosProveedores();
        refrescarDisciplinasDelCliente();
        refrescar = true;
    }

    /**
     * Inicio de las tablas de busqueda
     */
    public void inicarTablas() {
        iniciarTablaBuscarSocio();
        iniciarTablaBuscarProfesor();
        iniciarTablaBuscarProveedor();
        iniciarTablaBuscarIDSocio();
        iniciar = true;
    }


    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {

                    /*   CONTROL DE USUARIOS  */
        vistaPrincipal.adminBtn.addActionListener(listener);
        vistaPrincipal.comercialBtn.addActionListener(listener);
        vistaPrincipal.clienteBtn.addActionListener(listener);



        /*      VISTA COMERCIAL      */
        /* Botones de Login */
        vistaLoginComercial.inicarSesionComercialBtn.addActionListener(listener);
        vistaLoginComercial.volverBtn.addActionListener(listener);
        vistaLoginComercial.navegarRegistrarComercialBtn.addActionListener(listener);

        /* Registro nuevo Comercial */
        vistaRegistroComercial.registrarNuevoComercialBtn.addActionListener(listener);
        vistaRegistroComercial.volverBtn.addActionListener(listener);


        /* PEDIDOS AL PROVEEDOR */
        vistaComercial.comprarPedidoProveedorBtn.addActionListener(listener);
        vistaComercial.borrarPedidoProveedorBtn.addActionListener(listener);
        vistaComercial.modificarPedidoProveedorBtn.addActionListener(listener);
        vistaComercial.volverDesdePedidoBtn.addActionListener(listener);



        /*      VISTA CLIENTE       */
        /* Botones de Login */
        vistaLoginCliente.consultarDisciplinasClienteBtn.addActionListener(listener);
        vistaLoginCliente.volverBtn.addActionListener(listener);
        vistaCliente.volverDesdeClasesBtn.addActionListener(listener);
        vistaCliente.busquedaIDSocioBtn.addActionListener(listener);



        /*    VISTA ADMINISTRADOR  */
        /* Botones de Login */
        vistaLoginAdmin.inicioSesionAdminBtn.addActionListener(listener);
        vistaLoginAdmin.volverBtn.addActionListener(listener);

        /* SOCIOS */
        vistaAdministrador.anadirSocioBtn.addActionListener(listener);
        vistaAdministrador.borrarSocioBtn.addActionListener(listener);
        vistaAdministrador.modificarSocioBtn.addActionListener(listener);
        vistaAdministrador.buscarSocioBtn.addActionListener(listener);
        vistaAdministrador.volverDesdeSocioBtn.addActionListener(listener);

        /* PROFESORES */
        vistaAdministrador.anadirProfesorBtn.addActionListener(listener);
        vistaAdministrador.borrarProfesorBtn.addActionListener(listener);
        vistaAdministrador.modificarProfesorBtn.addActionListener(listener);
        vistaAdministrador.buscarProfesorBtn.addActionListener(listener);
        vistaAdministrador.volverDesdeProfesorBtn.addActionListener(listener);

        /* CLASES */
        vistaAdministrador.anadirClaseBtn.addActionListener(listener);
        vistaAdministrador.borrarClaseBtn.addActionListener(listener);
        vistaAdministrador.modificarClaseBtn.addActionListener(listener);
        vistaAdministrador.volverDesdeClaseBtn.addActionListener(listener);
        vistaAdministrador.generarFacturaBtn.addActionListener(listener);

        /* PROVEEDORES */
        vistaAdministrador.anadirProveedorBtn.addActionListener(listener);
        //vistaAdministrador.borrarProveedorBtn.addActionListener(listener);
        vistaAdministrador.modificarProveedorBtn.addActionListener(listener);
        vistaAdministrador.buscarProveedorBtn.addActionListener(listener);
        vistaAdministrador.volverDesdeProveedorBtn.addActionListener(listener);

    }

    /**
     *  Metodo que al seleccionar un campo en la tabla muestra cada uno de los campos en sus respectivas JTextField para ahi realizar la modificacion,
     *  la visualizacion hace que sea mas sencillo usas el metodo
     */
    public void mouseTableListener() {

            /* VISTA ADMINISTRADOR: */
        /*SOCIO*/
        vistaAdministrador.tablaSocio.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vistaAdministrador.tablaSocio.getSelectedRow();
                vistaAdministrador.nombreSocioTxt.setText(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 1)));
                vistaAdministrador.apellidoSocioTxt.setText(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 2)));
                vistaAdministrador.dniSocioTxt.setText(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 3)));
                vistaAdministrador.direccionSocioTxt.setText(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 4)));
                vistaAdministrador.ciudadSocioTxt.setText(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 5)));
                vistaAdministrador.fechaNacimientoSocioDate.setDate(Date.valueOf(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 6))).toLocalDate());
                vistaAdministrador.telefonoSocioTxt.setText(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 7)));
                vistaAdministrador.fechaInscripcionSocioDate.setDate(Date.valueOf(String.valueOf(vistaAdministrador.tablaSocio.getValueAt(row, 8))).toLocalDate());
            }
        });
            /* PROFESOR */
        vistaAdministrador.tablaProfesor.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vistaAdministrador.tablaProfesor.getSelectedRow();
                vistaAdministrador.nombreProfesorTxt.setText(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 1)));
                vistaAdministrador.apellidoProfesorTxt.setText(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 2)));
                vistaAdministrador.dniProfesorTxt.setText(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 3)));
                vistaAdministrador.direccionProfesorTxt.setText(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 4)));
                vistaAdministrador.ciudadProfesorTxt.setText(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 5)));
                vistaAdministrador.fechaNacimientoProfesorDate.setDate(Date.valueOf(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 6))).toLocalDate());
                vistaAdministrador.telefonoProfesorTxt.setText(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 7)));
                vistaAdministrador.comboBoxHorasProfesor.setSelectedItem(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 8)));
                vistaAdministrador.comboBoxDisciplinaProfesor.setSelectedItem(String.valueOf(vistaAdministrador.tablaProfesor.getValueAt(row, 9)));
            }
        });
            /* CLASE */
        vistaAdministrador.tablaClase.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vistaAdministrador.tablaClase.getSelectedRow();
                vistaAdministrador.comboBoxNombreSocioClases.setSelectedItem(String.valueOf(vistaAdministrador.tablaClase.getValueAt(row, 1)));
                vistaAdministrador.fechaReservaClaseDate.setDate(Date.valueOf(String.valueOf(vistaAdministrador.tablaClase.getValueAt(row, 2))).toLocalDate());
                vistaAdministrador.comboBoxTipoClases.setSelectedItem(String.valueOf(vistaAdministrador.tablaClase.getValueAt(row, 3)));
                vistaAdministrador.cantClasesTxt.setText(String.valueOf(vistaAdministrador.tablaClase.getValueAt(row, 4)));
                vistaAdministrador.metodoPagoTxt.setText(String.valueOf(vistaAdministrador.tablaClase.getValueAt(row, 5)));
                vistaAdministrador.comboBoxDisciplinaClase.setSelectedItem(String.valueOf(vistaAdministrador.tablaClase.getValueAt(row, 6)));
                vistaAdministrador.precioClaseTxt.setText(String.valueOf(vistaAdministrador.tablaClase.getValueAt(row, 7)));
                }
            });
            /* PROVEEDOR */
        vistaAdministrador.tablaProveedor.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vistaAdministrador.tablaProveedor.getSelectedRow();
                vistaAdministrador.nombreProveedorTxt.setText(String.valueOf(vistaAdministrador.tablaProveedor.getValueAt(row, 1)));
                vistaAdministrador.direccionProveedorTxt.setText(String.valueOf(vistaAdministrador.tablaProveedor.getValueAt(row, 2)));
                vistaAdministrador.ciudadProveedorTxt.setText(String.valueOf(vistaAdministrador.tablaProveedor.getValueAt(row, 3)));
                vistaAdministrador.emailProveedorTxt.setText(String.valueOf(vistaAdministrador.tablaProveedor.getValueAt(row, 4)));
                vistaAdministrador.fechaContratoProveedorDate.setDate(Date.valueOf(String.valueOf(vistaAdministrador.tablaProveedor.getValueAt(row, 5))).toLocalDate());
            }
        });
            /* VISTA COMERCIAL */
        vistaComercial.tablaPedidosProveedor.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vistaComercial.tablaPedidosProveedor.getSelectedRow();
                vistaComercial.comboBoxNombreProveedor.setSelectedItem(String.valueOf(vistaComercial.tablaPedidosProveedor.getValueAt(row, 1)));
                vistaComercial.cantPedidoProveedorTxt.setText(String.valueOf(vistaComercial.tablaPedidosProveedor.getValueAt(row, 2)));
                vistaComercial.fechaPedidoProveedorDate.setDate(Date.valueOf(String.valueOf(vistaComercial.tablaPedidosProveedor.getValueAt(row, 3))).toLocalDate());
                vistaComercial.comboBoxMaterialProveedor.setSelectedItem(String.valueOf(vistaComercial.tablaPedidosProveedor.getValueAt(row, 4)));
                vistaComercial.precioTotalPedidoTxt.setText(String.valueOf(vistaComercial.tablaPedidosProveedor.getValueAt(row, 5)));
            }
        });
    }

    /**
     * Metodo que da la accion correspondiente a cada boton
     * @param e recoge la accion del boton y la ejecuta en los case
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {

            /*     CONTROL DE USUARIOS: VISTA PRINCIPAL    */
            case "AdminAcceder":
                vistaLoginAdmin.setVisible(true);
                vistaPrincipal.setVisible(false);
                break;
            case "ComercialAcceder":
                vistaLoginComercial.setVisible(true);
                vistaPrincipal.setVisible(false);
                break;
            case "ClienteAcceder":
                vistaLoginCliente.setVisible(true);
                vistaPrincipal.setVisible(false);
                break;


            /* ADMINISTRADOR */
            case "InicioSesionAdmin":
                    modelo.conectarHipica();
                    if (comprobarLoginAdminVacio()) {
                        Util.showErrorAlert("Hay campos vacios, rellena todos los campos.");
                    } else {
                        try {
                            if (modelo.comprobarDatosAdministrador(
                                    vistaLoginAdmin.nombreAdminTxt.getText(),
                                    vistaLoginAdmin.passwordAdminTxt.getText())) {
                                vistaAdministrador.setVisible(true);
                                vistaLoginAdmin.setVisible(false);
                            } else {
                                Util.showErrorAlert("Los datos son incorrectos. No puedes acceder");
                                vistaLoginAdmin.setVisible(true);
                            }
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                        borrarCamposAdmin();
                    }
            break;
            case "volverVistaPrincipalAdmin":
                vistaPrincipal.setVisible(true);
                vistaLoginAdmin.setVisible(false);
                break;


            /* COMERCIAL */
            case "InicarSesionComercial":
                modelo.conectarHipica();
                if (comprobarLoginComercialVacio()) {
                    Util.showErrorAlert("Hay campos vacios, rellena todos los campos.");
                } else {
                    String contrasenaADescifrar = Hash.sha1(vistaLoginComercial.passwordComercialTxt.getText());
                    try {
                        if (modelo.comprobarDatosComercial(vistaLoginComercial.nombreComercialTxt.getText(), contrasenaADescifrar)) {
                            vistaComercial.setVisible(true);
                            vistaLoginComercial.setVisible(false);
                        } else {
                            Util.showErrorAlert("El usuario " + vistaLoginComercial.nombreComercialTxt.getText() + " no esta registrado.");
                            vistaLoginComercial.setVisible(false);
                            vistaRegistroComercial.setVisible(true);
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                    borrarCamposComercial();
                }
            break;
            case "volverVistaPrincipalComercial":
                vistaPrincipal.setVisible(true);
                vistaLoginComercial.setVisible(false);
                break;
            case "IrARegistrarNuevoComercial":
                vistaLoginComercial.setVisible(false);
                vistaRegistroComercial.setVisible(true);
                break;
            case "registrarNuevoComercial":
                try {
                    modelo.conectarHipica();
                    if (comprobarRegistroComercialVacio()) {
                        Util.showErrorAlert("Hay campos vacios, rellena todos los campos.");
                    }
                    else if (modelo.nombreComercialYaExiste(vistaRegistroComercial.nombreNuevoComerciaTxt.getText())) {
                        Util.showErrorAlert("El usuario " + vistaRegistroComercial.nombreNuevoComerciaTxt.getText() + " ya esta registrado en la base de datos." +
                                "\nIntroduce un nuevo usuario.");
                    } else {
                        /* Encripta la contraseña en la base de datos*/
                        String contrasena = new String(vistaRegistroComercial.passwordNuevoComercialTxt.getText());
                        String contrasenaCifrada = Hash.sha1(contrasena);

                        modelo.insertarDatosComercial(vistaRegistroComercial.nombreNuevoComerciaTxt.getText(),
                                contrasenaCifrada);

                        vistaRegistroComercial.setVisible(false);
                        vistaComercial.setVisible(true);
                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                borrarCamposComercial();
                break;
            case "volverDesdeRegistroComercial":
                vistaRegistroComercial.setVisible(false);
                vistaPrincipal.setVisible(true);
                break;



            /* CLIENTE */
            case "consultarLasDisciplinasCliente":
                modelo.conectarHipica();
                if(comprobarLoginClienteVacio()) {
                    Util.showErrorAlert("Selecciona un campo.");
                } else {
                    try {
                        if(modelo.comprobarDatosCliente((String) vistaLoginCliente.comboBoxDisciplinasClase.getSelectedItem())) {
                            String mostrarDisciplina = (String) vistaLoginCliente.comboBoxDisciplinasClase.getSelectedItem();
                            if(vistaLoginCliente.comboBoxDisciplinasClase.getSelectedItem().equals(Disciplina.DOMA_CLASICA.getValor())) {
                                try {
                                    ResultSet rs = modelo.buscarClasesDelCliente(mostrarDisciplina);
                                    cargarFilasCliente(rs);
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                                vistaCliente.setVisible(true);
                                vistaLoginCliente.setVisible(false);

                            } else if(vistaLoginCliente.comboBoxDisciplinasClase.getSelectedItem().equals(Disciplina.DOMA_VAQUERA.getValor())) {
                                try {
                                    ResultSet rs = modelo.buscarClasesDelCliente(mostrarDisciplina);
                                    cargarFilasCliente(rs);
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                                vistaCliente.setVisible(true);
                                vistaLoginCliente.setVisible(false);
                            } else if(vistaLoginCliente.comboBoxDisciplinasClase.getSelectedItem().equals(Disciplina.DOMA_PROGRESIVA.getValor())) {
                                try {
                                    ResultSet rs = modelo.buscarClasesDelCliente(mostrarDisciplina);
                                    cargarFilasCliente(rs);
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                                vistaCliente.setVisible(true);
                                vistaLoginCliente.setVisible(false);

                            } else if(vistaLoginCliente.comboBoxDisciplinasClase.getSelectedItem().equals(Disciplina.SALTO_OBSTACULOS.getValor())) {
                                try {
                                    ResultSet rs = modelo.buscarClasesDelCliente(mostrarDisciplina);
                                    cargarFilasCliente(rs);
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                                vistaCliente.setVisible(true);
                                vistaLoginCliente.setVisible(false);
                            } else if(vistaLoginCliente.comboBoxDisciplinasClase.getSelectedItem().equals(Disciplina.RAID.getValor())) {
                                try {
                                    ResultSet rs = modelo.buscarClasesDelCliente(mostrarDisciplina);
                                    cargarFilasCliente(rs);
                                } catch (SQLException ex) {
                                    ex.printStackTrace();
                                }
                                vistaCliente.setVisible(true);
                                vistaLoginCliente.setVisible(false);
                            }
                            else {
                                Util.showInfoAlert("No has seleccionado ningun campo");
                            }


                        } else {
                            Util.showErrorAlert("En esta disciplina aun no se apuntado nadie. ¡Se el primero!");
                            vistaLoginCliente.setVisible(true);
                            //vistaPrincipal.setVisible(true);
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
                    borrarCamposCliente();
                break;
            case "volverVistaPrincipalCliente":
                vistaPrincipal.setVisible(true);
                vistaLoginCliente.setVisible(false);
                break;

            case "busquedaIDSocio":
                String buscarId = vistaCliente.busquedaSocioTxt.getText();
                try {
                    ResultSet rs = modelo.buscarIDSocio(buscarId);
                    cargarFilasIDSocio(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;


                /* VOLVER A LA VISTA PRINCIPAL PULSANDO EN EL LOGOTIPO */
            case "volverDesdeSocio":
            case"volverDesdeProfesor":
            case "volverDesdeClase":
            case "volverDesdeProveedor":
                vistaPrincipal.setVisible(true);
                vistaAdministrador.setVisible(false);
                break;
            case "volverDesdePedido":
                vistaPrincipal.setVisible(true);
                vistaComercial.setVisible(false);
                break;
            case "volverDesdeClases":
                vistaPrincipal.setVisible(true);
                vistaCliente.setVisible(false);
                break;


            /*      AÑADIR SOCIO, PROFESOR, CLASE, PROVEEDOR Y LOS PEDIDOS REALIZADOS AL PROVEEDOR    */
            case "AnadirSocio": {
                try {
                    if (comprobarSocioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vistaAdministrador.tablaSocio.clearSelection();
                    } else if (modelo.dniSocioYaExiste(vistaAdministrador.dniSocioTxt.getText())) {
                        Util.showWarningAlert("El socio con DNI " + vistaAdministrador.dniSocioTxt.getText() + " ya esta registrado.\nIntroduce un nuevo socio");
                        vistaAdministrador.tablaSocio.clearSelection();
                    } else {
                        modelo.insertarSocio(
                                vistaAdministrador.nombreSocioTxt.getText(),
                                vistaAdministrador.apellidoSocioTxt.getText(),
                                vistaAdministrador.dniSocioTxt.getText(),
                                vistaAdministrador.direccionSocioTxt.getText(),
                                vistaAdministrador.ciudadSocioTxt.getText(),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaNacimientoSocioDate.getDate())),
                                Integer.parseInt(vistaAdministrador.telefonoSocioTxt.getText()),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaInscripcionSocioDate.getDate())));
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vistaAdministrador.tablaSocio.clearSelection();
                }
                borrarCamposSocio();
                refrescarSocios();
            }
            break;
            case "AnadirProfesor": {
                try {
                    if (comprobarProfesorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaAdministrador.tablaProfesor.clearSelection();
                    } else if (modelo.dniProfesorYaExiste(vistaAdministrador.dniProfesorTxt.getText())) {
                        Util.showWarningAlert("El profesor con DNI " + vistaAdministrador.dniProfesorTxt.getText() + " ya esta registrado." +
                                "\nIntroduce un nuevo Profesor");
                        vistaAdministrador.tablaProfesor.clearSelection();
                    } else {
                        modelo.insertarProfesor(
                                vistaAdministrador.nombreProfesorTxt.getText(),
                                vistaAdministrador.apellidoProfesorTxt.getText(),
                                vistaAdministrador.dniProfesorTxt.getText(),
                                vistaAdministrador.direccionProfesorTxt.getText(),
                                vistaAdministrador.ciudadProfesorTxt.getText(),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaNacimientoProfesorDate.getDate())),
                                Integer.parseInt(vistaAdministrador.telefonoProfesorTxt.getText()),
                                String.valueOf(vistaAdministrador.comboBoxHorasProfesor.getSelectedItem()),
                                String.valueOf(vistaAdministrador.comboBoxDisciplinaProfesor.getSelectedItem()));
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaAdministrador.tablaProfesor.clearSelection();
                }
                borrarCamposProfesor();
                refrescarProfesores();
            }
            break;

            case "AnadirClase": {
                try {
                    if (comprobarClaseVacia()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaAdministrador.tablaClase.clearSelection();
                    } else if (modelo.fechaClaseYaExiste(Date.valueOf(String.valueOf(vistaAdministrador.fechaReservaClaseDate.getDate())))) {
                        Util.showWarningAlert("La clase con esa fecha ya esta registrada\nIntroduce un nueva fecha.");
                        vistaAdministrador.tablaClase.clearSelection();
                    } else {
                        modelo.insertarClase(
                                String.valueOf(vistaAdministrador.comboBoxNombreSocioClases.getSelectedItem()),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaReservaClaseDate.getDate())),
                                String.valueOf(vistaAdministrador.comboBoxTipoClases.getSelectedItem()),
                                Integer.parseInt(vistaAdministrador.cantClasesTxt.getText()),
                                vistaAdministrador.metodoPagoTxt.getText(),
                                String.valueOf(vistaAdministrador.comboBoxDisciplinaClase.getSelectedItem()),
                                Integer.parseInt(vistaAdministrador.precioClaseTxt.getText()));
                        tieneClase = true;
                        claseSeleccionada = true;
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaAdministrador.tablaClase.clearSelection();
                }
                borrarCamposClase();
                refrescarClases();
            }
            break;
            /*GENERAR LA FACTURA DE LA CLASE ELEGIDA */
            case "GenerarFactura":
                try {
                    if(claseSeleccionada = true) {
                        // Carga del archivo .jasper
                        JasperReport reporte = (JasperReport) JRLoader.loadObject(new File("generarFactura/FacturaCompra.jasper"));
                        // crear conexion con la base de datos
                        Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/pegasohipica", "root", "");
                        // reporta los datos de la base de datos
                        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, null, conexion);

                        // El rporte sera guardado en formato PDF
                        JRExporter exporter = new JRPdfExporter();
                        // Se exportan los datos anteriores
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT,jasperPrint);
                        // Nombre del archivo que vamos a generar
                        exporter.setParameter(JRExporterParameter.OUTPUT_FILE,new java.io.File("generarFactura/FacturaCompraClases.pdf"));
                        // realiza el proceso de exportacion
                        exporter.exportReport();
                    } else {
                        Util.showErrorAlert("No has seleccionado ninguna clase, no se puede generar ninguna factura");
                    }
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;


            case "AnadirProveedor":{
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaAdministrador.tablaProveedor.clearSelection();
                    } else if (modelo.nombreProveedorYaExiste(vistaAdministrador.nombreProveedorTxt.getText())) {
                        Util.showWarningAlert("El proveedor " + vistaAdministrador.nombreProveedorTxt.getText() + " ya esta registrado.\nIntroduce un nuevo proveedor.");
                        vistaAdministrador.tablaProveedor.clearSelection();
                    } else {
                        modelo.insertarProveedor(
                                vistaAdministrador.nombreProveedorTxt.getText(),
                                vistaAdministrador.direccionProveedorTxt.getText(),
                                vistaAdministrador.ciudadProveedorTxt.getText(),
                                vistaAdministrador.emailProveedorTxt.getText(),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaContratoProveedorDate.getDate())));
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaAdministrador.tablaProveedor.clearSelection();
                }
                borrarCamposProveedor();
                refrescarProveedores();
            }
            break;
            case "ComprarPedidoProveedor": {
                try {
                    if (comprobarPedidoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaComercial.tablaPedidosProveedor.clearSelection();
                    } else {
                        modelo.insertarPedidoProveedor(
                                String.valueOf(vistaComercial.comboBoxNombreProveedor.getSelectedItem()),
                                Integer.parseInt(vistaComercial.cantPedidoProveedorTxt.getText()),
                                Date.valueOf(String.valueOf(vistaComercial.fechaPedidoProveedorDate.getDate())),
                                String.valueOf(vistaComercial.comboBoxMaterialProveedor.getSelectedItem()),
                                Integer.parseInt(vistaComercial.precioTotalPedidoTxt.getText()));
                        tienePedido = true;
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaComercial.tablaPedidosProveedor.clearSelection();
                }
                borrarCamposPedidos();
                refrescarPedidosProveedores();
            }
            break;


            /*      MODIFICAR SOCIO, PROFESOR, CLASE Y PROVEEDOR Y LOS PEDIDOS REALIZADOS AL PROVEEDOR    */
            case "ModificarSocio": {
                try {
                    if (comprobarSocioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaAdministrador.tablaSocio.clearSelection();
                    } else {
                        modelo.modificarSocio(
                                vistaAdministrador.nombreSocioTxt.getText(),
                                vistaAdministrador.apellidoSocioTxt.getText(),
                                vistaAdministrador.dniSocioTxt.getText(),
                                vistaAdministrador.direccionSocioTxt.getText(),
                                vistaAdministrador.ciudadSocioTxt.getText(),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaNacimientoSocioDate.getDate())),
                                Integer.parseInt(vistaAdministrador.telefonoSocioTxt.getText()),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaInscripcionSocioDate.getDate())),
                                Integer.parseInt((String) vistaAdministrador.tablaSocio.getValueAt(vistaAdministrador.tablaSocio.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaAdministrador.tablaSocio.clearSelection();
                }
                borrarCamposSocio();
                refrescarSocios();
            }
            break;
            case "ModificarProfesor": {
                try {
                    if (comprobarProfesorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaAdministrador.tablaProfesor.clearSelection();
                    } else {
                        modelo.modificarProfesor(
                                vistaAdministrador.nombreProfesorTxt.getText(),
                                vistaAdministrador.apellidoProfesorTxt.getText(),
                                vistaAdministrador.dniProfesorTxt.getText(),
                                vistaAdministrador.direccionProfesorTxt.getText(),
                                vistaAdministrador.ciudadProfesorTxt.getText(),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaNacimientoProfesorDate.getDate())),
                                Integer.parseInt(vistaAdministrador.telefonoProfesorTxt.getText()),
                                String.valueOf(vistaAdministrador.comboBoxHorasProfesor.getSelectedItem()),
                                String.valueOf(vistaAdministrador.comboBoxDisciplinaProfesor.getSelectedItem()),
                                Integer.parseInt((String) vistaAdministrador.tablaProfesor.getValueAt(vistaAdministrador.tablaProfesor.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaAdministrador.tablaProfesor.clearSelection();
                }
                borrarCamposProfesor();
                refrescarProfesores();
            }
            break;
            case "ModificarClase": {
                try {
                    if (comprobarClaseVacia()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaAdministrador.tablaClase.clearSelection();
                    } else {
                        modelo.modificarClase(
                                String.valueOf(vistaAdministrador.comboBoxNombreSocioClases.getSelectedItem()),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaReservaClaseDate.getDate())),
                                String.valueOf(vistaAdministrador.comboBoxTipoClases.getSelectedItem()),
                                Integer.parseInt(vistaAdministrador.cantClasesTxt.getText()),
                                vistaAdministrador.metodoPagoTxt.getText(),
                                String.valueOf(vistaAdministrador.comboBoxDisciplinaClase.getSelectedItem()),
                                Integer.parseInt(vistaAdministrador.precioClaseTxt.getText()),
                                Integer.parseInt((String) vistaAdministrador.tablaClase.getValueAt(vistaAdministrador.tablaClase.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaAdministrador.tablaClase.clearSelection();
                }
                borrarCamposClase();
                refrescarClases();
            }
            break;
            case "ModificarProveedor": {
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaAdministrador.tablaProveedor.clearSelection();
                    } else {
                        modelo.modificarProveedor(
                                vistaAdministrador.nombreProveedorTxt.getText(),
                                vistaAdministrador.direccionProveedorTxt.getText(),
                                vistaAdministrador.ciudadProveedorTxt.getText(),
                                vistaAdministrador.emailProveedorTxt.getText(),
                                Date.valueOf(String.valueOf(vistaAdministrador.fechaContratoProveedorDate.getDate())),
                                Integer.parseInt((String) vistaAdministrador.tablaProveedor.getValueAt(vistaAdministrador.tablaProveedor.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaAdministrador.tablaProveedor.clearSelection();
                }
                borrarCamposProveedor();
                refrescarProveedores();
            }
            break;
            case "ModificarPedidoProveedor": {
                try {
                    if (comprobarPedidoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos.");
                        vistaComercial.tablaPedidosProveedor.clearSelection();
                    } else {
                        modelo.modificarPedidoProveedor(
                                String.valueOf(vistaComercial.comboBoxNombreProveedor.getSelectedItem()),
                                Integer.parseInt(vistaComercial.cantPedidoProveedorTxt.getText()),
                                Date.valueOf(String.valueOf(vistaComercial.fechaPedidoProveedorDate.getDate())),
                                String.valueOf(vistaComercial.comboBoxMaterialProveedor.getSelectedItem()),
                                Integer.parseInt(vistaComercial.precioTotalPedidoTxt.getText()),
                                Integer.parseInt((String) vistaComercial.tablaPedidosProveedor.getValueAt(vistaComercial.tablaPedidosProveedor.getSelectedRow(), 0)));
                        tienePedido = true;
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                    Util.showErrorAlert("Introduce números en los campos que lo requieren.");
                    vistaComercial.tablaPedidosProveedor.clearSelection();
                }
                borrarCamposPedidos();
                refrescarPedidosProveedores();
            }
            break;


            /*      ELIMINAR SOCIO, PROFESOR, CLASE, PROVEEDOR Y LOS PEDIDOS REALIZADOS AL PROVEEDOR    */
            case "BorrarSocio":
                if (tieneClase = true) {
                    Util.showErrorAlert("El Socio " + vistaAdministrador.nombreSocioTxt.getText() + " " + vistaAdministrador.apellidoSocioTxt.getText() + " tiene clases contratadas, no se puede dar de baja");
                } else {
                    modelo.borrarSocio(Integer.parseInt((String) vistaAdministrador.tablaSocio.getValueAt(vistaAdministrador.tablaSocio.getSelectedRow(), 0)));
                }
                borrarCamposSocio();
                refrescarSocios();
                break;
            case "BorrarProfesor":
                modelo.borrarProfesor(Integer.parseInt((String) vistaAdministrador.tablaProfesor.getValueAt(vistaAdministrador.tablaProfesor.getSelectedRow(), 0)));
                borrarCamposProfesor();
                refrescarProfesores();
                break;
            case "BorrarClase":
                modelo.borrarClase(Integer.parseInt((String) vistaAdministrador.tablaClase.getValueAt(vistaAdministrador.tablaClase.getSelectedRow(), 0)));
                borrarCamposClase();
                refrescarClases();
                break;
            /*case "BorrarProveedor":
                if(tienePedido = false) {
                    modelo.borrarProveedor(Integer.parseInt((String) vistaAdministrador.tablaProveedor.getValueAt(vistaAdministrador.tablaProveedor.getSelectedRow(), 0)));
                } else {
                   Util.showErrorAlert("El Proveedor " + vistaAdministrador.nombreProveedorTxt.getText() +" tiene pedidos realizados, no se puede dar de baja.");
                }
                borrarCamposProveedor();
                refrescarProveedores();
                break;*/
           case "BorrarPedidoProveedor":
                modelo.borrarPedidoComercialAlProveedor(Integer.parseInt((String) vistaComercial.tablaPedidosProveedor.getValueAt(vistaComercial.tablaPedidosProveedor.getSelectedRow(), 0)));
                borrarCamposPedidos();
                refrescarPedidosProveedores();
                break;


            /*      BUSCAR POR DIFERENTES ELEMENTOS AL SOCIO, PROFESOR Y PROVEEDOR   */
            case "BuscarTelefonoSocio":
                String buscarTelefonoSocio = vistaAdministrador.buscarTelefonoSocio.getText();
                try {
                    ResultSet rs = modelo.buscarTelefonoSocio(buscarTelefonoSocio);
                    cargarFilasSocio(rs);
                    refrescarSocios();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "BuscarDniProfesor":
                String buscarDniProfesor = vistaAdministrador.buscarDniProfesor.getText();
                try {
                    ResultSet rs = modelo.buscarDniProfesor(buscarDniProfesor);
                    cargarFilasProfesor(rs);
                } catch(SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "BuscarNombreProveedor":
                String buscarCiudadproveedor = vistaAdministrador.buscarCiudadProveedor.getText();
                try {
                    ResultSet rs = modelo.buscarCiudadProveedor(buscarCiudadproveedor);
                    cargarFilasProveedor(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            default:
        }
    }

    /*      ACTUALIZACION DE LAS LISTAS DE CADA ENTIDAD      */
    /*SOCIOS */
    /** Actualiza los Socios que se ven en la lista y los comboboxs */
    private void refrescarSocios() {
        try {
            vistaAdministrador.tablaSocio.setModel(construirTableModelSocio(modelo.consultarSocio()));
            vistaAdministrador.comboBoxNombreSocioClases.removeAllItems();
            for (int i = 0; i < vistaAdministrador.dtmSocios.getRowCount(); i++) {
                vistaAdministrador.comboBoxNombreSocioClases.addItem(
                        vistaAdministrador.dtmSocios.getValueAt(i, 0) + "-" + vistaAdministrador.dtmSocios.getValueAt(i, 1) + " "
                                + vistaAdministrador.dtmSocios.getValueAt(i, 2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /* PROFESORES */
    /** Actualiza los Profesores que se ven en la lista y los comboboxes */
    private void refrescarProfesores() {
        try {
            vistaAdministrador.tablaProfesor.setModel(construirTableModelProfesor(modelo.consultarProfesor()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /* CLASES */
    /** Actualiza las Clases que se ven en la lista y los comboboxes */
   private void refrescarClases() {
        try {
            vistaAdministrador.tablaClase.setModel(construirTableModelClase(modelo.consultarClase()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /* PROVEEDOR */
    /** Actualiza los Proveedores que se ven en la lista y los comboboxes */
    private void refrescarProveedores() {
        try {
            vistaAdministrador.tablaProveedor.setModel(construirTableModelProveedor(modelo.consultarProveedor()));
            vistaComercial.comboBoxNombreProveedor.removeAllItems();
            for (int i = 0; i < vistaAdministrador.dtmProveedores.getRowCount(); i++) {
                vistaComercial.comboBoxNombreProveedor.addItem(
                        vistaAdministrador.dtmProveedores.getValueAt(i, 0) + "-" + vistaAdministrador.dtmProveedores.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* PEDIDOS AL PROVEEDOR */
    /** Actualiza los Pedidos al proveedores que se ven en la lista y los comboboxes */
    private void refrescarPedidosProveedores() {
        try {
            vistaComercial.tablaPedidosProveedor.setModel(construirTableModelPedidoProveedor(modelo.consultarPedidosComercialAlProveedor()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* DISCIPLINAS DE LOS SOCIOS*/
    /** Actualiza las Clases del Socio/Cliente que se ven en la lista y los comboboxes */
    private void refrescarDisciplinasDelCliente() {
        try {
            vistaCliente.tablaDisciplinaClasesSocios.setModel(construirTableModelDisicplinaDelCliente(modelo.consultarDisciplinasDelSocio()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Metodo que carga cada uno de los campos de la tabla Socio de la base de datos
     * @param rs retorna el resultado
     * @return vistaAdministrador.dtmSocios
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelSocio(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vistaAdministrador.dtmSocios.setDataVector(data, columnNames);
        return vistaAdministrador.dtmSocios;
    }

    /**
     * Metodo que carga cada uno de los campos de la tabla Profesor de la base de datos
     * @param rs retorna el resultado
     * @return vistaAdministrador.dtmProfesores
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelProfesor(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vistaAdministrador.dtmProfesores.setDataVector(data, columnNames);
        return vistaAdministrador.dtmProfesores;
    }
    /**
     * Metodo que carga cada uno de los campos de la tabla Clase de la base de datos
     * @param rs retorna el resultado
     * @return vistaAdministrador.dtmClases
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelClase(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vistaAdministrador.dtmClases.setDataVector(data, columnNames);
        return vistaAdministrador.dtmClases;
    }
    /**
     * Metodo que carga cada uno de los campos de la tabla Proveedor de la base de datos
     * @param rs retorna el resultado
     * @return vistaAdministrador.dtmProveedores
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelProveedor(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vistaAdministrador.dtmProveedores.setDataVector(data, columnNames);
        return vistaAdministrador.dtmProveedores;
    }
    /**
     * Metodo que carga cada uno de los campos de la tabla Comercial que realiza Pedidos al Profesor de la base de datos
     * @param rs retorna el resultado
     * @return vistaComercial.dtmPedidosProveedor
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelPedidoProveedor(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vistaComercial.dtmPedidosProveedor.setDataVector(data, columnNames);
        return  vistaComercial.dtmPedidosProveedor;
    }
    /**
     * Metodo que carga cada uno de los campos de la tabla Disciplina del Socio de la base de datos
     * @param rs retorna el resultado
     * @return vistaCliente.dtmClasesSocio
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelDisicplinaDelCliente(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vistaCliente.dtmClasesSocio.setDataVector(data, columnNames);
        return vistaCliente.dtmClasesSocio;
    }

    /**
     * Metodo que recoge cada columna de las tablas de la base de datos
     * @param rs retorna el resultado
     * @param columnCount
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }



    /* COMPROBAR SI LOS CAMPOS ESTAN VACIOS DE SOCIO, PROFESOR, CLASES Y PROVEEDOR */
    /**
     * Comprueba que los campos necesarios para añadir un Socio están vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarSocioVacio() {
        return vistaAdministrador.nombreSocioTxt.getText().isEmpty() ||
                vistaAdministrador.apellidoSocioTxt.getText().isEmpty() ||
                vistaAdministrador.dniSocioTxt.getText().isEmpty() ||
                vistaAdministrador.direccionSocioTxt.getText().isEmpty() ||
                vistaAdministrador.ciudadSocioTxt.getText().isEmpty() ||
                vistaAdministrador.fechaNacimientoSocioDate.getText().isEmpty() ||
                vistaAdministrador.telefonoSocioTxt.getText().isEmpty() ||
                vistaAdministrador.fechaInscripcionSocioDate.getText().isEmpty();
    }
    /**
     * Comprueba que los campos necesarios para añadir un Profesor están vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarProfesorVacio() {
        return vistaAdministrador.nombreProfesorTxt.getText().isEmpty() ||
                vistaAdministrador.apellidoProfesorTxt.getText().isEmpty() ||
                vistaAdministrador.dniProfesorTxt.getText().isEmpty() ||
                vistaAdministrador.direccionProfesorTxt.getText().isEmpty() ||
                vistaAdministrador.ciudadProfesorTxt.getText().isEmpty() ||
                vistaAdministrador.fechaNacimientoProfesorDate.getText().isEmpty() ||
                vistaAdministrador.telefonoProfesorTxt.getText().isEmpty() ||
                vistaAdministrador.comboBoxHorasProfesor.getSelectedIndex() == -1 ||
                vistaAdministrador.comboBoxDisciplinaProfesor.getSelectedIndex() == -1 ;
    }
    /**
     * Comprueba que los campos necesarios para añadir una Clase están vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarClaseVacia() {
        return vistaAdministrador.comboBoxNombreSocioClases.getSelectedIndex() == -1 ||
                vistaAdministrador.fechaReservaClaseDate.getText().isEmpty() ||
                vistaAdministrador.comboBoxTipoClases.getSelectedIndex() == -1 ||
                vistaAdministrador.cantClasesTxt.getText().isEmpty() ||
                vistaAdministrador.metodoPagoTxt.getText().isEmpty() ||
                vistaAdministrador.comboBoxDisciplinaClase.getSelectedIndex() == -1 ||
                vistaAdministrador.precioClaseTxt.getText().isEmpty();
    }
    /**
     * Comprueba que los campos necesarios para añadir un Proveedor están vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarProveedorVacio() {
        return vistaAdministrador.nombreProveedorTxt.getText().isEmpty() ||
                vistaAdministrador.direccionProveedorTxt.getText().isEmpty() ||
                vistaAdministrador.ciudadProveedorTxt.getText().isEmpty() ||
                vistaAdministrador.emailProveedorTxt.getText().isEmpty() ||
                vistaAdministrador.fechaContratoProveedorDate.getText().isEmpty();
    }


    /* COMPROBAR LOS DATOS VACIOS DE LA VISTA DE PEDIDOS AL PROVEEDOR*/
    /**
     * Comprueba que los campos necesarios para realizar un Pedido al proveedor si están vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarPedidoVacio() {
        return vistaComercial.comboBoxNombreProveedor.getSelectedIndex() == -1 ||
                vistaComercial.cantPedidoProveedorTxt.getText().isEmpty() ||
                vistaComercial.fechaPedidoProveedorDate.getText().isEmpty() ||
                vistaComercial.comboBoxMaterialProveedor.getSelectedIndex() == -1 ||
                vistaComercial.precioTotalPedidoTxt.getText().isEmpty();
    }


                    /*    CONTROL DE USUARIOS      */
    /**
     * Comprueba que los campos necesarios no esten vacios para iniciar sesion un ADMINISTRADOR si están vacíos
     * @return True si al menos uno de los campos está vacío
     */
    public boolean comprobarLoginAdminVacio() {
        return vistaLoginAdmin.nombreAdminTxt.getText().isEmpty() ||
                vistaLoginAdmin.passwordAdminTxt.getText().isEmpty();
    }
    /**
     * Comprueba que los campos necesarios no esten vacios para iniciar sesion un COMERCIAL
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarLoginComercialVacio() {
        return vistaLoginComercial.nombreComercialTxt.getText().isEmpty() ||
                vistaLoginComercial.passwordComercialTxt.getText().isEmpty();
    }
    /**
     * Comprueba que el campo DNI no este vacio para acceder a la vista de CLIENTE
     * @return True si al menos uno de los campos está vacío
     */
    boolean comprobarRegistroComercialVacio() {
        return vistaRegistroComercial.nombreNuevoComerciaTxt.getText().isEmpty() ||
                vistaRegistroComercial.passwordNuevoComercialTxt.getText().isEmpty();
    }
    /**
     * Comprueba que el campo combobox no este vacio para acceder a la vista de CLIENTE
     * @return True si al menos uno de los campos está vacío
     */
    boolean comprobarLoginClienteVacio() {
        return vistaLoginCliente.comboBoxDisciplinasClase.getSelectedIndex() == -1;
    }





        /*   BORRA TODOS LOS CAMPOS DE SOCIO, PROFESOR, CLASES, PROVEEDOR Y LOS PEDIDOS  */
    /** Vacía los campos de la tabla Socios */
    private void borrarCamposSocio() {
        vistaAdministrador.nombreSocioTxt.setText("");
        vistaAdministrador.apellidoSocioTxt.setText("");
        vistaAdministrador.dniSocioTxt.setText("");
        vistaAdministrador.direccionSocioTxt.setText("");
        vistaAdministrador.ciudadSocioTxt.setText("");
        vistaAdministrador.fechaNacimientoSocioDate.setText("");
        vistaAdministrador.telefonoSocioTxt.setText("");
        vistaAdministrador.fechaInscripcionSocioDate.setText("");
    }
    /** Vacía los campos de la tabla Profesor */
    private void borrarCamposProfesor() {
        vistaAdministrador.nombreProfesorTxt.setText("");
        vistaAdministrador.apellidoProfesorTxt.setText("");
        vistaAdministrador.dniProfesorTxt.setText("");
        vistaAdministrador.direccionProfesorTxt.setText("");
        vistaAdministrador.ciudadProfesorTxt.setText("");
        vistaAdministrador.fechaNacimientoProfesorDate.setText("");
        vistaAdministrador.telefonoProfesorTxt.setText("");
        vistaAdministrador.comboBoxHorasProfesor.setSelectedIndex(-1);
        vistaAdministrador.comboBoxDisciplinaProfesor.setSelectedIndex(-1);
    }
    /** Vacía los campos de la tabla Clase */
    private void borrarCamposClase() {
        vistaAdministrador.comboBoxNombreSocioClases.setSelectedIndex(-1);
        vistaAdministrador.fechaReservaClaseDate.setText("");
        vistaAdministrador.comboBoxTipoClases.setSelectedIndex(-1);
        vistaAdministrador.cantClasesTxt.setText("");
        vistaAdministrador.metodoPagoTxt.setText("");
        vistaAdministrador.comboBoxDisciplinaClase.setSelectedIndex(-1);
        vistaAdministrador.precioClaseTxt.setText("");
    }
    /** Vacía los campos de la tabla Proveedor */
    private void borrarCamposProveedor() {
        vistaAdministrador.nombreProveedorTxt.setText("");
        vistaAdministrador.direccionProveedorTxt.setText("");
        vistaAdministrador.ciudadProveedorTxt.setText("");
        vistaAdministrador.emailProveedorTxt.setText("");
        vistaAdministrador.fechaContratoProveedorDate.setText("");
    }

    /* BORRAR TODOS LOS CAMPOS EN LA TABLA DE PEDIDOS AL PROVEEDOR */
    /** Vacía los campos de la tabla Pedidos al proveedor */
    private void borrarCamposPedidos() {
        vistaComercial.comboBoxNombreProveedor.setSelectedIndex(-1);
        vistaComercial.cantPedidoProveedorTxt.setText("");
        vistaComercial.fechaPedidoProveedorDate.setText("");
        vistaComercial.comboBoxMaterialProveedor.setSelectedIndex(-1);
        vistaComercial.precioTotalPedidoTxt.setText("");
    }

    /*BORRAR TODOS LOS CAMPOS AL CONTROL DE USUARIOS */
    /** Vacía los campos deL ADMINISTRADOR  */
    private void borrarCamposAdmin() {
        vistaLoginAdmin.nombreAdminTxt.setText("");
        vistaLoginAdmin.passwordAdminTxt.setText("");
    }
    /** Vacía los campos deL ADMINISTRADOR  */
    private void borrarCamposComercial() {
        vistaLoginComercial.nombreComercialTxt.setText("");
        vistaLoginComercial.passwordComercialTxt.setText("");
    }
    /** Vacía los campos deL ADMINISTRADOR  */
    private void borrarCamposCliente() {
        vistaLoginCliente.comboBoxDisciplinasClase.setSelectedIndex(-1);
    }




    /* INICAR LAS TABLAS Y CARGARLAS DE CADA FILTRO DE LAS TABLAS: SOCIO, PROFESOR, PROVEEDOR, CLIENTE E ID DEL SOCIO */
    /** Metodo que inica la tabla de busqueda de los Socios. Mostrara el dni, nombre y apellido del socio */
    private void iniciarTablaBuscarSocio() {
        String[] headers = { "Telefono", "Nombre", "Apellido" };
        vistaAdministrador.dtmSociosBusqueda.setColumnIdentifiers(headers);
    }
    /**
     * Carga las filas que se mostraran en la tabla de busqueda del Socio
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasSocio(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[3];
        vistaAdministrador.dtmSociosBusqueda.setRowCount(0);
        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            vistaAdministrador.dtmSociosBusqueda.addRow(fila);
        }
    }

    /** Metodo que inica la tabla de busqueda de los Profesor. Mostrara el dni, nombre y apellido del socio */
    private void iniciarTablaBuscarProfesor() {
        String[] headers = { "DNI", "Nombre", "Apellido" };
        vistaAdministrador.dtmProfesoresBusqueda.setColumnIdentifiers(headers);
    }
    /**
     * Carga las filas que se mostraran en la tabla de busqueda del Profesor
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasProfesor(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[3];
        vistaAdministrador.dtmProfesoresBusqueda.setRowCount(0);
        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            vistaAdministrador.dtmProfesoresBusqueda.addRow(fila);
        }
    }

    /** Metodo que inica la tabla de busqueda de los Proveedores. Mostrara el nombre del roveedor y su direccion */
    private void iniciarTablaBuscarProveedor() {
        String[] headers = { "Nombre", "Dirección", "Ciudad" };
        vistaAdministrador.dtmProveedoresBusqueda.setColumnIdentifiers(headers);
    }
    /**
     * Carga las filas que se mostraran en la tabla de busqueda del Proveedor
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasProveedor(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[3];
        vistaAdministrador.dtmProveedoresBusqueda.setRowCount(0);
        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            vistaAdministrador.dtmProveedoresBusqueda.addRow(fila);
        }
    }


     /** Carga las filas que se mostraran en la tabla las clases contratadas del Socio
     * @param resultSet
     * @throws SQLException
      */
   private void cargarFilasCliente(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[4];
        vistaCliente.dtmClasesSocio.setRowCount(0);
        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            vistaCliente.dtmClasesSocio.addRow(fila);
        }
    }

    /** Metodo que inica la tabla de busqueda de los Socios. Mostrara el dni, nombre y apellido del socio */
    private void iniciarTablaBuscarIDSocio() {
        String[] headers = { "ID", "Nombre", "Apellido" };
        vistaCliente.dtmBusquedaSocio.setColumnIdentifiers(headers);
    }
    /**
     * Carga las filas que se mostraran en la tabla de busqueda del Socio
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilasIDSocio(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[3];
        vistaCliente.dtmBusquedaSocio.setRowCount(0);
        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            vistaCliente.dtmBusquedaSocio.addRow(fila);
        }
    }

    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) {
        vistaAdministrador.addWindowListener(listener);
    }

    /**
     * Añade acciones cuando la ventana se cierra. Si el autoguardado está activado la ventana se cerrará
     * sin más, volcando todos los datos en el fichero. De lo contrario pueden ocurrir dos cosas:
     * Si todos los cambios han sido guardados, la ventana se cerrará. Si no lo están, aparecerá una ventana
     * que permitirá al usuario decidir que hacer con esos cambios no guardados o bien cancelar el cierre
     * de la aplicación.
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    /*     ¡LISTENERS IMPLEMENTADOS NO UTILIZADOS!    */
    public void itemStateChanged(ItemEvent itemEvent) { }

    @Override
    public void windowOpened(WindowEvent windowEvent) { }
    @Override
    public void windowClosed(WindowEvent windowEvent) { }
    @Override
    public void windowIconified(WindowEvent windowEvent) { }
    @Override
    public void windowDeiconified(WindowEvent windowEvent) { }
    @Override
    public void windowActivated(WindowEvent windowEvent) { }
    @Override
    public void windowDeactivated(WindowEvent windowEvent) { }

    @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) { }
}
