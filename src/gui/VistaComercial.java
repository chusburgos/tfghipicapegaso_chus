package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.TipoMaterial;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class VistaComercial extends JFrame {

    private final static String TITULOFRAME = "REALIZAR PEDIDOS: HIPICA PEGASO SL";
    public JPanel panelComercial;


    /* PEDIDOS AL PROVEEDOR */
    public JComboBox comboBoxNombreProveedor;
    public JTextField cantPedidoProveedorTxt;
    public DatePicker fechaPedidoProveedorDate;
    public JComboBox comboBoxMaterialProveedor;
    public JTextField precioTotalPedidoTxt;

    // botones
    public JButton comprarPedidoProveedorBtn;
    public JButton borrarPedidoProveedorBtn;
    public JButton modificarPedidoProveedorBtn;
    public JButton volverDesdePedidoBtn;

    // tabla y lista de proveedores
    public JTable tablaPedidosProveedor;

    /* Default table models */
    public DefaultTableModel dtmPedidosProveedor;


    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaComercial() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hacra visible cuando la llamen desde el controlador
     */
    public void initFrame() {
        this.setContentPane(panelComercial);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
        volverDesdePedidoBtn.setBorder(null);
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    public void setTableModels() {
        this.dtmPedidosProveedor = new DefaultTableModel();
        this.tablaPedidosProveedor.setModel(dtmPedidosProveedor);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        // Material que pediremos al proveedor los cuales tengamos registrados en la base de datos
        for(TipoMaterial constant : TipoMaterial.values()) { comboBoxMaterialProveedor.addItem(constant.getvalorMaterial()); }
        comboBoxMaterialProveedor.setSelectedIndex(-1);

    }


}
