package gui;

import enums.Disciplina;

import javax.swing.*;

public class VistaLoginCliente extends JFrame {

    private final static String TITULOFRAME = "CLASES CLIENTE: HIPICA PEGASO SL";
    public JPanel panelLoginCliente;

    /* DATOS PARA INICIAR SESION */
    public JComboBox<String> comboBoxDisciplinasClase;

    /* BOTONES */
    public JButton volverBtn;
    public JButton consultarDisciplinasClienteBtn;


    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaLoginCliente() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hara visible cuando la llamen desde el controlador
     */
    private void initFrame() {
        this.setContentPane(panelLoginCliente);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        volverBtn.setBorder(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
        setEnumComboBox();
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        // Disciplina que tiene de especialidad el profesor
        for (Disciplina constant : Disciplina.values()) { comboBoxDisciplinasClase.addItem(constant.getValor()); }
        comboBoxDisciplinasClase.setSelectedIndex(-1);
    }

}
