package gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class VistaCliente extends JFrame {
    private final static String TITULOFRAME = "CLASES CLIENTE: HIPICA PEGASO SL";
    public JPanel panelCliente;

    public JTextField busquedaSocioTxt;

    /* Boton para volver a la pantalla principal*/
    public JButton volverDesdeClasesBtn;
    public JButton busquedaIDSocioBtn;

    /* Tabla donde apareceran las clases compradas por el socio */
    public JTable tablaDisciplinaClasesSocios;
    public JTable tablaBusquedaId;

    /* Default table models */
    public DefaultTableModel dtmClasesSocio;
    public DefaultTableModel dtmBusquedaSocio;


    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaCliente() {
        super(TITULOFRAME);
        initFrame();
    }


    /**
     * Configura la ventana y la hara visible cuando la llamen en el controlador
     */
    public void initFrame() {
        this.setContentPane(panelCliente);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
        volverDesdeClasesBtn.setBorder(null);
        busquedaIDSocioBtn.setBorder(null);
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    public void setTableModels() {
        /* SOCIOS */
        this.dtmClasesSocio = new DefaultTableModel();
        this.tablaDisciplinaClasesSocios.setModel(dtmClasesSocio);

        /* Busqueda del ID del socio*/
        this.dtmBusquedaSocio = new DefaultTableModel();
        this.tablaBusquedaId.setModel(dtmBusquedaSocio);
    }


}