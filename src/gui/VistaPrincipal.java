package gui;


import javax.swing.*;

public class VistaPrincipal extends JFrame {
    private final static String TITULOFRAME = "HIPICA PEGASO SL";
    public JPanel panelVentanaInicio;

    /* Botones que navegaran a las demas ventanas*/
    public  JButton adminBtn;
    public JButton comercialBtn;
    public JButton clienteBtn;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaPrincipal() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    public void initFrame() {
        this.setContentPane(panelVentanaInicio);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
    }

}
