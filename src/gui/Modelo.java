package gui;

import java.io.*;
import java.sql.*;

public class Modelo {

    private Connection conexion;

    /* CONECTAR LA BASE DE DATOS DE LA HIPICA */
    /**
     * Metodo que crea la conexion con la base de datos
     */
    void conectarHipica() {
        try {
            /* Conexion con la base de datos de la hipica */
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/pegasohipica", "root", "");
        } catch (SQLException sqle) {
            try {
                /* Conexion con la base de datos de la hipica */
                conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/pegasohipica", "root", "");

                PreparedStatement statement = null;

                String code = leerFicheroHipica();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Leera los datos del fichero y los almacenara en la base de datos
     * @return la cadena de texto
     * @throws IOException
     */
    private String leerFicheroHipica() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("BBDDHipica_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * Este metodo comprueba si el nombre del ADMINISTRADOR ya esta en la base de datos
     * @param nombre nombre del Administrador
     * @param contrasena contraseña del Administrador
     * @return true si ya existe
     */
    public boolean comprobarDatosAdministrador(String nombre, String contrasena) throws SQLException {
        if (conexion == null)
            return false;
        if (conexion.isClosed())
            return false;

        String consulta = "SELECT * FROM datosadministrador WHERE nombre_admin = '" + nombre + "' " +
                "AND contrasena_admin = '" + contrasena + "'";

        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        while(resultado.next()){ return true; }

        if (sentencia != null) {  sentencia.close(); }
        return false;
    }

    /**
     * Este metodo comprueba si el nombre del COMERCIAL ya esta en la base de datos
     * @param nombre nombre del Comercial
     * @param contrasena contraseña del Comercial
     * @return true si ya existe
     */
    public boolean comprobarDatosComercial(String nombre, String contrasena) throws SQLException {
        if (conexion == null)
            return false;
        if (conexion.isClosed())
            return false;

        String consulta = "SELECT * FROM datoscomercial WHERE nombre_comercial = '" + nombre + "' " +
                "AND contrasena_comercial = '" + contrasena + "'";

        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        while(resultado.next()){ return true; }

        if (sentencia != null) {  sentencia.close(); }
        return false;
    }

    /**
     * Metodo de REGISTRO de un nuevo COMERCIAL, introduciendo el nombre y la constraseña
     * @param nombre nombre del nuevo comercial que realizara los pedidos
     * @param contrasena contraseña del nuevo comercial que realizara los pedidos
     * @return numeroRegistros
     * @throws SQLException
     */
    public int insertarDatosComercial(String nombre, String contrasena) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta = "INSERT INTO datoscomercial (nombre_comercial, contrasena_comercial) VALUES (?,?)";

        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setString(2, contrasena);
        int numeroRegistros = sentencia.executeUpdate();
        if (sentencia != null) { sentencia.close(); }

        return numeroRegistros;
    }

    /**
     * Este metodo comprueba si la DISCIPLINA de las clases que contrata el CLIENTE ya esta en la base de datos
     * @param disciplina disciplina que impartiran los clientes
     * @return true si ya existe
     */
    public boolean comprobarDatosCliente(String disciplina) throws SQLException {
        if (conexion == null)
            return false;
        if (conexion.isClosed())
            return false;

        String consulta = "SELECT disciplina FROM clase WHERE disciplina = '" + disciplina + "'";

        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        while(resultado.next()){ return true; }
        if (sentencia != null) {  sentencia.close(); }
        return false;
    }


    /* COLOCACION DE LOS DATOS EN LA TABLA DE LA BASE DE DATOS */
    /**
     * Metodo que devuelve todos los Socios de la base de datos
     * @return consulta recoge los datos de la base de datos
     * @throws SQLException
     */
    ResultSet consultarSocio() throws SQLException {
        String sentenciaSql = "SELECT concat(socio.idsocio) as 'ID', concat(socio.nombre) as 'Nombre', concat(socio.apellido) as 'Apellido', " +
                "concat(socio.dni) as 'DNI', concat(socio.direccion) as 'Direccion', concat(socio.ciudad) as 'Ciudad', " +
                "concat(socio.fecha_nacimiento) as 'Fecha Nacimiento', concat(socio.telefono) as 'Telofono', " +
                "concat(socio.fecha_inscripcion) as 'Fecha Inscripcion' " +
                "FROM socio";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Metodo que devuelve todos los Profesores de la base de datos
     * @return consulta recoge los datos de la base de datos
     * @throws SQLException
     */
    ResultSet consultarProfesor() throws SQLException {
        String sentenciaSql = "SELECT concat(profe.idprofesor) as 'ID', concat(profe.nombre) as 'Nombre', concat(profe.apellido) as 'Apellido', " +
                "concat(profe.dni) as 'DNI', concat(profe.direccion) as 'Direccion', concat(profe.ciudad) as 'Ciudad', " +
                "concat(profe.fecha_nacimiento) as 'Fecha Nacimiento', concat(profe.telefono) as 'Telofono', " +
                "concat(profe.numero_horas) as 'Numero Horas', concat(profe.disciplina) as 'Disciplina' FROM profesor as profe";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Metodo que devuelve todas las Clases de la base de datos
     * @return consulta recoge los datos de la base de datos
     * @throws SQLException
     */
    ResultSet consultarClase() throws SQLException {
        String sentenciaSql = "SELECT concat(idclase) as 'ID', concat(s.nombre, ' ', s.apellido) as 'Socio', " +
                "concat(fecha_reserva) as 'Fecha Reserva', concat(tipo_clase) as 'Tipo Clase', " +
                "concat(cantidad_clases) as 'Cantidad Clases', concat(metodo_pago) as 'Metodo Pago', " +
                "concat(disciplina) as 'Disciplina', concat(precio) as 'Precio' " +
                "FROM clase as clase " +
                "INNER JOIN socio as s on s.idsocio = clase.idsocio";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Metodo que devuelve las Disciplinas que tiene reservadas el Socio de la base de datos
     * @return consulta recoge los datos de la base de datos
     * @throws SQLException
     */
    ResultSet consultarDisciplinasDelSocio() throws SQLException {
        String sentenciaSql = "SELECT concat(idclase) as 'ID Socio', concat(s.nombre, ' ', s.apellido) as 'Fecha Reserva', " +
                "concat(fecha_reserva) as 'Tipo Clase', concat(tipo_clase) as 'Disciplina' " +
                "FROM clase as c " +
                "INNER JOIN socio as s on s.idsocio = c.idsocio";
        PreparedStatement sentencia;
        ResultSet resultado;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Metodo que devuelve todos los Proveedores de la base de datos
     * @return consulta recoge los datos de la base de datos
     * @throws SQLException
     */
    ResultSet consultarProveedor() throws SQLException {
        String sentenciaSql = "SELECT concat(proveedor.idproveedor) as 'ID', concat(proveedor.nombre) as 'Nombre', " +
                "concat(proveedor.direccion) as 'Direccion', concat(proveedor.ciudad) as 'Ciudad', " +
                "concat(proveedor.email) as 'Email', concat(proveedor.fecha_contrato) as 'Fecha Contrato' " +
                "FROM proveedor";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Metodo que devuelve todos los pedidos que se han realizado al proveedor seleccionado de la base de datos
     * @return consulta recoge los datos de la base de datos
     * @throws SQLException
     */
    ResultSet consultarPedidosComercialAlProveedor() throws SQLException {
        String sentenciaSql = "SELECT concat(idcomercial) as 'ID', concat(pro.nombre) as 'Nombre Proveedor', " +
                "concat(cantidad_pedido) as 'Cantidad Pedido', concat(fecha_pedido) as 'Fecha Pedido', " +
                "concat(tipo_material) as 'Tipo Material', concat(precio) as 'Precio' " +
                "FROM comercial as co " +
                "INNER JOIN proveedor as pro on pro.idproveedor = co.idproveedor";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }



    /*      ¡METODOS DE INSERTAR!      */
    /** Inserta los datos de cada Socio, todos los parametros
     * @param nombre_socio nombre del socio
     * @param apellido_socio primer apellido del socio
     * @param dni_socio dni unico por cada socio
     * @param direccion_socio direccion del socio
     * @param ciudad_socio ciudad del socio
     * @param fecha_nacimiento_socio fecha de nacimiento del socio
     * @param telefono_socio telefono del socio
     * @param fecha_inscripcion_socio fecha en la que se inscribe el socio
     */
    void insertarSocio(String nombre_socio, String apellido_socio, String dni_socio, String direccion_socio, String ciudad_socio,
                       Date fecha_nacimiento_socio, int telefono_socio, Date fecha_inscripcion_socio) {
        String sentenciaSql = "INSERT INTO socio (nombre, apellido, dni, direccion, ciudad, fecha_nacimiento, telefono, " +
                "fecha_inscripcion) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_socio);
            sentencia.setString(2, apellido_socio);
            sentencia.setString(3, dni_socio);
            sentencia.setString(4, direccion_socio);
            sentencia.setString(5, ciudad_socio);
            sentencia.setDate(6, Date.valueOf(String.valueOf(fecha_nacimiento_socio)));
            sentencia.setString(7, String.valueOf(telefono_socio));
            sentencia.setDate(8, Date.valueOf(String.valueOf(fecha_inscripcion_socio)));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Inserta los datos de cada Profesor de la hipica, todos los parametros
     * @param nombre_profesor nombre del profesor
     * @param apellido_profesor primer apellido del profesor
     * @param dni_profesor dni unico de cada profesor
     * @param direccion_profesor direccion del profesor
     * @param ciudad_profesor ciudad del profesor
     * @param fecha_nacimiento_profesor fecha de nacimiento del profesor
     * @param telefono_profesor telofono del profesor
     * @param numero_horas_profesor numero de horas que esta contratado el profesor
     * @param disciplina_profesor disciplina que imparte el profesor
     */
    void insertarProfesor(String nombre_profesor, String apellido_profesor, String dni_profesor, String direccion_profesor, String ciudad_profesor,
                          Date fecha_nacimiento_profesor, int telefono_profesor, String numero_horas_profesor, String disciplina_profesor) {
        String sentenciaSql = "INSERT INTO profesor (nombre, apellido, dni, direccion, ciudad, fecha_nacimiento, telefono, " +
                "numero_horas, disciplina) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_profesor);
            sentencia.setString(2, apellido_profesor);
            sentencia.setString(3, dni_profesor);
            sentencia.setString(4, direccion_profesor);
            sentencia.setString(5, ciudad_profesor);
            sentencia.setDate(6, Date.valueOf(String.valueOf(fecha_nacimiento_profesor)));
            sentencia.setString(7, String.valueOf(telefono_profesor));
            sentencia.setString(8, numero_horas_profesor);
            sentencia.setString(9, disciplina_profesor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Inserta los datos de cada Clase, todos los parametros
     * @param socioClase id del socio que comprara la clase, ira a su nombre la factura
     * @param fecha_reserva fecha de la reserva de la clase
     * @param tipo_clase elegira si es individual o grupal la clase
     * @param cantidad_clases la cantidad de clases que quiere comprar
     * @param metodo_pago metodo de pago con el que hara el importe de la clase
     * @param disciplina la disciplina que elegira el socio
     * @param precio precio de la clase comprada por el socio
     */
    void insertarClase(String socioClase, Date fecha_reserva, String tipo_clase, int cantidad_clases, String metodo_pago, String disciplina, int precio) {
        String sentenciaSql = "INSERT INTO clase (idsocio, fecha_reserva, tipo_clase, cantidad_clases, metodo_pago, disciplina, precio)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        int idsocio = Integer.valueOf(socioClase.split("-")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, String.valueOf(idsocio));
            sentencia.setDate(2, fecha_reserva);
            sentencia.setString(3, tipo_clase);
            sentencia.setInt(4, cantidad_clases);
            sentencia.setString(5, metodo_pago);
            sentencia.setString(6, disciplina);
            sentencia.setInt(7, precio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Inserta los datos de cada Proveedor, todos los parametros
     * @param nombre_proveedor nombre del proveedor que facilita el material
     * @param direccion direccion del proveedor
     * @param ciudad ciudad donde se localiza el proveedor
     * @param email email del proveedor por si fuera necesario el contacto con ellos
     * @param fecha_contrato fecha de registro de cuando empiezan a trabajar con el
     */
    void insertarProveedor(String nombre_proveedor, String direccion, String ciudad, String email, Date fecha_contrato) {
        String sentenciaSql = "INSERT INTO proveedor (nombre, direccion, ciudad, email, fecha_contrato) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_proveedor);
            sentencia.setString(2, direccion);
            sentencia.setString(3, ciudad);
            sentencia.setString(4, email);
            sentencia.setDate(5, Date.valueOf(String.valueOf(fecha_contrato)));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Inserta los datos de cada Pedido del Proveedor, todos los parametros
     * @param proveedorComercial nombre del proveedor ya registrado
     * @param cantidad_pedido cantidad de pedido de compra
     * @param fecha_pedido fecha de pedido de la compra
     * @param tipo_material tipo del material que proporciona el proveedor
     * @param precio precio del pedido al proveedor
     */
    void insertarPedidoProveedor(String proveedorComercial, int cantidad_pedido, Date fecha_pedido, String tipo_material, int precio) {
        String sentenciaSql = "INSERT INTO comercial (idproveedor, cantidad_pedido, fecha_pedido, tipo_material, precio) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        int idproveedor = Integer.valueOf(proveedorComercial.split("-")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, String.valueOf(idproveedor));
            sentencia.setInt(2, Integer.parseInt(String.valueOf(cantidad_pedido)));
            sentencia.setDate(3, Date.valueOf(String.valueOf(fecha_pedido)));
            sentencia.setString(4, tipo_material);
            sentencia.setInt(5, Integer.parseInt(String.valueOf(precio)));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    /*      ¡METODOS DE MODIFICAR!     */
    /**
     *  Metodo que modifica Socio elegido
     *  la casilla de la tabla donde aparezca el parametro que se haya introducido previamente
     * @param nombre_socio nombre del socio
     * @param apellido_socio apellido del socio
     * @param dni_socio dni unico del socio
     * @param direccion_socio direccion del socio
     * @param ciudad_socio ciudad del socio
     * @param fecha_nacimiento_socio fecha de nacimiento del socio
     * @param telefono_socio telefono del socio
     * @param fecha_inscripcion_socio fecha de inscripcion en la hipica
     * @param idsocio id del socio, id unico
     */
    void modificarSocio(String nombre_socio, String apellido_socio, String dni_socio, String direccion_socio, String ciudad_socio,
                        Date fecha_nacimiento_socio, int telefono_socio, Date fecha_inscripcion_socio, int idsocio){
        String sentenciaSql = "UPDATE socio SET nombre =?, apellido =?, dni =?, direccion =?, ciudad =?, fecha_nacimiento =?, telefono =?, " +
                "fecha_inscripcion =? WHERE idsocio =?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_socio);
            sentencia.setString(2, apellido_socio);
            sentencia.setString(3, dni_socio);
            sentencia.setString(4, direccion_socio);
            sentencia.setString(5, ciudad_socio);
            sentencia.setDate(6, Date.valueOf(String.valueOf(fecha_nacimiento_socio)));
            sentencia.setInt(7, telefono_socio);
            sentencia.setDate(8, Date.valueOf(String.valueOf(fecha_inscripcion_socio)));
            sentencia.setInt(9, idsocio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     *  Metodo que modifica Profesor elegido
     *  la casilla de la tabla donde aparezca el parametro que se haya introducido previamente
     * @param nombre_profesor nombre del profesor
     * @param apellido_profesor primer apellido del profesor
     * @param dni_profesor dni unico de cada profesor
     * @param direccion_profesor direccion del profesor
     * @param ciudad_profesor ciudad del profesor
     * @param fecha_nacimiento_profesor fecha de nacimiento del profesor
     * @param telefono_profesor telefono del profesor
     * @param numero_horas numero de horas que esta contratado el profesor
     * @param disciplina_profesor disciplina que imparte el profesor
     * @param idprofesor id unico de cada profesor
     */
    void modificarProfesor(String nombre_profesor, String apellido_profesor, String dni_profesor, String direccion_profesor,
                           String ciudad_profesor, Date fecha_nacimiento_profesor, int telefono_profesor, String numero_horas, String disciplina_profesor, int idprofesor){
        String sentenciaSql = "UPDATE profesor SET nombre =?, apellido =?, dni =?, direccion =?, ciudad =?, fecha_nacimiento =?, telefono =?, " +
                "numero_horas =?, disciplina =? WHERE idprofesor =?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_profesor);
            sentencia.setString(2, apellido_profesor);
            sentencia.setString(3, dni_profesor);
            sentencia.setString(4, direccion_profesor);
            sentencia.setString(5, ciudad_profesor);
            sentencia.setDate(6, Date.valueOf(String.valueOf(fecha_nacimiento_profesor)));
            sentencia.setInt(7, telefono_profesor);
            sentencia.setString(8, numero_horas);
            sentencia.setString(9, disciplina_profesor);
            sentencia.setInt(10, idprofesor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Metodo que modifica una Clase seleccionada
     * la casilla de la tabla donde aparezca el parametro que se haya introducido previamente
     * @param socio el nombre del socio que reservara/comprara la clase
     * @param fecha_reserva fecha de la clase que se reserva
     * @param tipo_clase tipo de clase si la clase sera individual o grupal
     * @param cantidad_clases la cantidad de clase que se comprara el socio
     * @param metodo_pago metodo de pago con el que se comprara la/s clase/s
     * @param disciplina la disciplina elegida por el socio
     * @param precio precio de la clase
     * @param idclase id unico de cada clase
     */
    void modificarClase(String socio, Date fecha_reserva, String tipo_clase, int cantidad_clases, String metodo_pago,
                        String disciplina, int precio, int idclase){
        String sentenciaSql = "UPDATE clase SET idsocio =?, fecha_reserva =?, tipo_clase =?, cantidad_clases =?, metodo_pago =?, disciplina =?, precio =? " +
                "WHERE idclase =?";
        PreparedStatement sentencia = null;
        int idsocio = Integer.valueOf(socio.split("-")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idsocio);
            sentencia.setDate(2, fecha_reserva);
            sentencia.setString(3, tipo_clase);
            sentencia.setInt(4, cantidad_clases);
            sentencia.setString(5, metodo_pago);
            sentencia.setString(6, disciplina);
            sentencia.setInt(7, precio);
            sentencia.setInt(8, idclase);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Metodo que modifica el Proveedor seleccionado
     * la casilla de la tabla donde aparezca el parametro que se haya introducido previamente
     * @param nombre_proveedor nombre del proveedor que facilita el material
     * @param direccion_proveedor direccion del proveedor
     * @param ciudad_proveedor ciudad donde se localiza el proveedor
     * @param email_proveedor email del proveedor por si fuera necesario el contacto con ellos
     * @param fecha_contrato_proveedor fecha de registro de cuando empiezan a trabajar con el
     */
    void modificarProveedor(String nombre_proveedor, String direccion_proveedor, String ciudad_proveedor, String email_proveedor, Date fecha_contrato_proveedor, int idproveedor){
        String sentenciaSql = "UPDATE proveedor SET nombre =?, direccion =?, ciudad =?, email =?, fecha_contrato =? WHERE idproveedor =?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre_proveedor);
            sentencia.setString(2, direccion_proveedor);
            sentencia.setString(3, ciudad_proveedor);
            sentencia.setString(4, email_proveedor);
            sentencia.setDate(5, Date.valueOf(String.valueOf(fecha_contrato_proveedor)));
            sentencia.setInt(6, idproveedor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Metodo que modifica un Pedido del proveedor
     * la casilla de la tabla donde aparezca el parametro que se haya introducido previamente
     * @param proveedor nombre del proveedor que facilita el material
     * @param cantidad_pedido cantidad de pedido de compra
     * @param fecha_pedido fecha de pedido de la compra
     * @param tipo_material tipo del material que proporciona el proveedor
     * @param precio precio del pedido al proveedor
     */
    void modificarPedidoProveedor(String proveedor, int cantidad_pedido, Date fecha_pedido, String tipo_material, int precio, int idcomercial){
        String sentenciaSql = "UPDATE comercial SET idproveedor =?, cantidad_pedido =?, fecha_pedido =?, tipo_material =?, precio =? WHERE idcomercial =?";
        PreparedStatement sentencia = null;
        int idproveedor = Integer.valueOf(proveedor.split("-")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idproveedor);
            sentencia.setInt(2, cantidad_pedido);
            sentencia.setDate(3, Date.valueOf(String.valueOf(fecha_pedido)));
            sentencia.setString(4, tipo_material);
            sentencia.setInt(5, precio);
            sentencia.setInt(6, idcomercial);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }



    /*      ¡METODOS DE BORRAR Y DAR DE BAJA!      */
    /**
     * Metodo que elimina un Socio
     * @param idsocio id del socio
     */
    void borrarSocio(int idsocio) {
        String sentenciaSql = "DELETE FROM socio WHERE idsocio = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idsocio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Metodo que elimina un Profesor
     * @param idprofesor id del profesor
     */
    void borrarProfesor(int idprofesor) {
        String sentenciaSql = "DELETE FROM profesor WHERE idprofesor = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idprofesor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Metodo que elimina una Clase
     * @param idclase id de la clase
     */
    void borrarClase(int idclase) {
        String sentenciaSql = "DELETE FROM clase WHERE idclase =?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idclase);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Metodo que elimina un Proveedor
     * @param idproveedor id del proveedor
     */
    void borrarProveedor(int idproveedor) {
        String sentenciaSql = "DELETE FROM proveedor WHERE idproveedor = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idproveedor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Metodo que elimina un Pedido al proveedor
     * @param idcomercial id del pedido del comerical al proveedor
     */
    void borrarPedidoComercialAlProveedor(int idcomercial) {
        String sentenciaSql = "DELETE FROM comercial WHERE idcomercial = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcomercial);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }




    /*      ¡METODOS DE COMPROBACION DE DATOS EN LA BBDD!      */
    /**
     * Este metodo comprueba si el dni del Socio ya esta en la base de datos
     * @param dni DNI del socio
     * @return true si ya existe
     */
    public boolean dniSocioYaExiste(String dni) {
        String consultadnisocio = "SELECT existeDniSocio(?)";
        PreparedStatement function;
        boolean dniSocioExists = false;
        try {
            function = conexion.prepareStatement(consultadnisocio);
            function.setString(1, dni);
            ResultSet rs = function.executeQuery();
            rs.next();

            dniSocioExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dniSocioExists;
    }
    /**
     * Este metodo comprueba si el dni del Profesor ya esta en la base de datos
     * @param dni DNI del profesor
     * @return true si ya existe
     */
    public boolean dniProfesorYaExiste(String dni) {
        String consultadniprofe = "SELECT existeDniProfesor(?)";
        PreparedStatement function;
        boolean dniProfesorExists = false;
        try {
            function = conexion.prepareStatement(consultadniprofe);
            function.setString(1, dni);
            ResultSet rs = function.executeQuery();
            rs.next();

            dniProfesorExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dniProfesorExists;
    }
    /**
     * Este metodo comprueba si el Socio ya tiene una Clase registrada y cuando
     * @param fecha fecha de la clase comprada
     * @return true si ya existe
     */
    public boolean fechaClaseYaExiste(Date fecha) {
        String consultafecha = "SELECT existeFechaRerserva(?)";
        PreparedStatement function;
        boolean fechaClaseYaExists = false;
        try {
            function = conexion.prepareStatement(consultafecha);
            function.setString(1, String.valueOf(fecha));
            ResultSet rs = function.executeQuery();
            rs.next();

            fechaClaseYaExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fechaClaseYaExists;
    }
    /**
     * Este metodo comprueba si el Nombre del Proveedor ya esta en la base de datos
     * @param nombre nombre del proveedor ya existente
     * @return true si ya existe
     */
    public boolean nombreProveedorYaExiste(String nombre) {
        String consultanombreproveedor = "SELECT existeNombreProveedor(?)";
        PreparedStatement function;
        boolean nombreProveedorYaExists = false;
        try {
            function = conexion.prepareStatement(consultanombreproveedor);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nombreProveedorYaExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombreProveedorYaExists;
    }

    /* CONTROL USUARIOS: REGISTRO DE UN NUEVO COMERCIAL */
    /**
     * Este metodo comprueba si el Nombre del Usuario ya esta en la base de datos
     * @param nombre nombre del usuario ya existente
     * @return true si ya existe
     */
    public boolean nombreComercialYaExiste(String nombre) {
        String consultanombre = "SELECT existeNombreComercial(?)";
        PreparedStatement function;
        boolean nombreComercialYaExists = false;
        try {
            function = conexion.prepareStatement(consultanombre);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nombreComercialYaExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombreComercialYaExists;
    }



    /* BUSCAR POR CAMPOS ESPECIFICOS DE CADA TABLA: SOCIOS, PROFESORES, CLASES Y PROVEEDORES */
    /**
     * Metodo que busca por el telefono del Socio, si esta registrado en la base de datos se muestra
     * @return busca los datos de la base de datos
     * @throws SQLException
     * @param telefono
     */
    public ResultSet buscarTelefonoSocio(String telefono) throws SQLException {
        if(conexion == null)
            return null;
        if(conexion.isClosed())
            return null;

        String consulta = "SELECT telefono, nombre, apellido FROM socio WHERE telefono =?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, telefono);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }
    /**
     * Metodo que busca por el dni si el Profesor si esta registrado en la base de datos se muestra
     * @return  busca los datos de la base de datos
     * @throws SQLException
     */
    public ResultSet buscarDniProfesor(String dni) throws SQLException {
        if(conexion == null)
            return null;
        if(conexion.isClosed())
            return null;

        String consulta = "SELECT dni, nombre, apellido FROM profesor WHERE dni =?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, dni);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }
    /**
     * Metodo que busca por Ciudad, muestra los Proveedores que estan registradas
     * @return  busca los datos de la base de datos
     * @throws SQLException
     */
    public ResultSet buscarCiudadProveedor(String ciudad) throws SQLException {
        if(conexion == null)
            return null;
        if(conexion.isClosed())
            return null;

        String consulta = "SELECT nombre, direccion, ciudad FROM proveedor WHERE ciudad =?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, ciudad);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Metodo que busca por la Discplina de las clases que tiene compradas del Cliente
     * @return  busca los datos de la base de datos
     * @throws SQLException
     */
    public ResultSet buscarClasesDelCliente(String disciplina) throws SQLException {
        if(conexion == null)
            return null;
        if(conexion.isClosed())
            return null;

        String consulta = "SELECT idsocio, fecha_reserva, tipo_clase, disciplina " +
                "FROM clase WHERE disciplina =? ";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, disciplina);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }
    /**
     * Metodo que busca por el ID del Socio, si esta registrado en la base de datos se muestra
     * @return  busca los datos de la base de datos
     * @throws SQLException
     * @param id
     */
    public ResultSet buscarIDSocio(String id) throws SQLException {
        if(conexion == null)
            return null;
        if(conexion.isClosed())
            return null;

        String consulta = "SELECT idsocio, nombre, apellido FROM socio WHERE idsocio =?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, id);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }




}
