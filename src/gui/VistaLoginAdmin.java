package gui;

import javax.swing.*;

public class VistaLoginAdmin extends JFrame {

    private final static String TITULOFRAME = "ADMINISTRADOR: HIPICA PEGASO SL";
    public  JPanel panelLoginAdmin;

    /* DATOS PARA INICIAR SESION */
    public JTextField nombreAdminTxt;
    public JPasswordField passwordAdminTxt;

    /* BOTONES */
    public JButton inicioSesionAdminBtn;
    public JButton volverBtn;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaLoginAdmin() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hara visible cuando la llamen desde el controlador
     */
    private void initFrame() {
        this.setContentPane(panelLoginAdmin);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        volverBtn.setBorder(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
    }





}
