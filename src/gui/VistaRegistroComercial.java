package gui;

import javax.swing.*;

public class VistaRegistroComercial extends JFrame{

    private final static String TITULOFRAME = "REGISTRO COMERCIAL: HIPICA PEGASO SL";
    public JPanel panelRegistroComercial;

    /* DATOS PARA REGISTRARSE */
    public JTextField nombreNuevoComerciaTxt;
    public JPasswordField passwordNuevoComercialTxt;

    /* BOTONES */
    public JButton volverBtn;
    public JButton registrarNuevoComercialBtn;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaRegistroComercial() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setContentPane(panelRegistroComercial);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        volverBtn.setBorder(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
    }

}
