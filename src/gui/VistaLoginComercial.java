package gui;

import javax.swing.*;

public class VistaLoginComercial extends JFrame{

    private final static String TITULOFRAME = "COMERCIAL: HIPICA PEGASO SL";
    public JPanel panelLoginComercial;

    /* DATOS PARA INICIAR SESION */
    public JTextField nombreComercialTxt;
    public JPasswordField passwordComercialTxt;

    /* BOTONES */
    public JButton volverBtn;
    public JButton inicarSesionComercialBtn;
    public JButton navegarRegistrarComercialBtn;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public VistaLoginComercial() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hara visible cuando la llamen del controlador
     */
    private void initFrame() {
        this.setContentPane(panelLoginComercial);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setLocationRelativeTo(null);
        volverBtn.setBorder(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/icono.PNG")).getImage());
    }


}
