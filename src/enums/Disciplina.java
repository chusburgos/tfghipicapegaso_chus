package enums;

/**
 * Clase Enumerada con los valores de los tipos de Disciplina
 */
public enum Disciplina {

    DOMA_CLASICA("Doma Clasica"),
    DOMA_VAQUERA("Doma Vaquera"),
    DOMA_PROGRESIVA("Doma Progresiva"),
    SALTO_OBSTACULOS("Salto de Obstaculos"),
    RAID("Raid");

    private String valor;

    Disciplina(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
