package enums;

/**
 * Clase enumerada que devuelve los diferentes tipos de materiales que nos proporciona el proveedor
 */
public enum TipoMaterial {

    SILLA("Sillas"),
    BOTAS("Botas"),
    CASCO("Cascos"),
    RIENDAS("Riendas"),
    ESTRIBOS("Estribos"),
    CABEZADA("Cabezadas"),
    GUANTES("Guantes"),
    EMBOCADURAS("Embocaduras");


    private String valorMaterial;

    TipoMaterial(String valorMaterial) {
        this.valorMaterial = valorMaterial;
    }

    public String getvalorMaterial() {
        return valorMaterial;
    }
}
