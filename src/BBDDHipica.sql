CREATE DATABASE IF NOT EXISTS pegasohipica;
--
USE pegasohipica;
--
CREATE TABLE IF NOT EXISTS socio (
    idsocio INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(30),
    apellido VARCHAR(40),
    dni VARCHAR(10) NOT NULL UNIQUE,
    direccion VARCHAR(50),
    ciudad VARCHAR(50),
    fecha_nacimiento DATE,
    telefono INT,
    fecha_inscripcion DATE
);
--
CREATE TABLE IF NOT EXISTS profesor (
    idprofesor INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(30),
    apellido VARCHAR(40),
    dni VARCHAR(10) NOT NULL UNIQUE,
    direccion VARCHAR(50),
    ciudad VARCHAR(50),
    fecha_nacimiento DATE,
    telefono INT,
    numero_horas Integer,    
    disciplina VARCHAR(50)
);
--
CREATE TABLE IF NOT EXISTS clase (
    idclase INT PRIMARY KEY AUTO_INCREMENT,
    idsocio INT(50),
    fecha_reserva DATE,
    tipo_clase VARCHAR(30), 
    cantidad_clases INT,   
    metodo_pago VARCHAR(30), 
    disciplina VARCHAR(50),
    precio INT
);
--
CREATE TABLE IF NOT EXISTS proveedor (
    idproveedor INT PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(100),
	direccion VARCHAR(100),
	ciudad VARCHAR(100),
	email VARCHAR(100),
	fecha_contrato DATE
);
--
CREATE TABLE IF NOT EXISTS comercial (
	idcomercial INT PRIMARY KEY AUTO_INCREMENT,
	idproveedor INT(50),
	cantidad_pedido INT,
	fecha_pedido DATE,
	tipo_material VARCHAR(50),
	precio INT
); delimiter ||
--
ALTER TABLE clase ADD FOREIGN KEY (idsocio) REFERENCES socio(idsocio); ||
delimiter ;
--
delimiter ||
ALTER TABLE comercial ADD FOREIGN KEY (idproveedor) REFERENCES proveedor(idproveedor); ||
delimiter ;
--
CREATE TABLE IF NOT EXISTS datosadministrador (
	idadmin INT PRIMARY KEY AUTO_INCREMENT,
	nombre_admin VARCHAR(50),
	contrasena_admin VARCHAR(50)
);
--
insert into datosadministrador (nombre_admin, contrasena_admin) values ('chus', '1122');
--
CREATE TABLE IF NOT EXISTS datoscomercial (
	idcomercial INT  PRIMARY KEY AUTO_INCREMENT,
	nombre_comercial VARCHAR(50),
	contrasena_comercial VARCHAR(50)
);
--
delimiter ||
CREATE FUNCTION existeDniSocio(funcion_dnisocio VARCHAR(40))
RETURNS BIT
BEGIN
	DECLARE i INT;
    SET i = 0;
    WHILE (i < (SELECT max(idsocio) FROM socio)) DO
    IF((SELECT dni FROM socio WHERE idsocio = (i + 1)) LIKE funcion_dnisocio) THEN RETURN 1;
    END IF;
    SET i = i + 1;
    END WHILE;
    RETURN 0;
END; ||
delimiter ;
--
delimiter ||
CREATE FUNCTION existeDniProfesor(funcion_dniprofesor VARCHAR(40))
RETURNS BIT
BEGIN
	DECLARE i INT;
    SET i = 0;
    WHILE (i < (SELECT max(idprofesor) FROM profesor)) DO
    IF((SELECT dni FROM profesor WHERE idprofesor = (i + 1)) LIKE funcion_dniprofesor) THEN RETURN 1;
    END IF;
    SET i = i + 1;
    END WHILE;
    RETURN 0;
END; ||
delimiter ;
--
delimiter ||
CREATE FUNCTION existeFechaRerserva(funcion_fecha DATE)
RETURNS BIT
BEGIN
	DECLARE i INT;
    SET i = 0;
    WHILE (i < (SELECT max(idclase) FROM clase)) DO
    IF((SELECT fecha_reserva FROM clase WHERE idclase = (i + 1)) LIKE funcion_fecha) THEN RETURN 1;
    END IF;
    SET i = i + 1;
    END WHILE;
    RETURN 0;
END; ||
delimiter ;
--
delimiter ||
CREATE FUNCTION existeNombreProveedor(funcion_nombreproveedor VARCHAR(100))
RETURNS BIT
BEGIN
	DECLARE i INT;
    SET i = 0;
    WHILE (i < (SELECT max(idproveedor) FROM proveedor)) DO
    IF((SELECT nombre FROM proveedor WHERE idproveedor = (i + 1)) LIKE funcion_nombreproveedor) THEN RETURN 1;
    END IF;
    SET i = i + 1;
    END WHILE;
    RETURN 0;
END; ||
delimiter ;
--
delimiter ||
CREATE FUNCTION existeNombreComercial(funcion_nombrecomercial VARCHAR(100))
RETURNS BIT
BEGIN
	DECLARE i INT;
    SET i = 0;
    WHILE (i < (SELECT max(idcomercial) FROM datoscomercial)) DO
    IF((SELECT nombre_comercial FROM datoscomercial WHERE idcomercial = (i + 1)) LIKE funcion_nombrecomercial) THEN RETURN 1;
    END IF;
    SET i = i + 1;
    END WHILE;
    RETURN 0;
END;
